const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    entry: './src/main.ts',
    output: {
        filename: './js/bundle.js',
        path: path.join(__dirname, 'dist')
    },
    resolve: {
        extensions:['.ts','.js']
    },
    devServer: {
        contentBase: path.join(__dirname,'dist')
    },
    module: {
        rules: [
            {test:/\.ts$/,loader:'ts-loader'},
            {test:/\.json$/,loader:'json-loader',type:'javascript/auto'}
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'The World Order',
            filename: 'index.html',
            template: './src/layout/title.html'
        }),
        new HtmlWebpackPlugin({
            title: 'The World Order',
            filename: 'game.html',
            template: './src/layout/game.html'
        })
    ]
}