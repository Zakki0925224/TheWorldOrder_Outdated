export interface ScenariosData {
  scenarios: Scenario[]
}

export interface Scenario {
  title: string
  start_date: string
  countries: Country[]
  events: Event[]
}

export interface Country {
  code: string
  name: string
  government?: Government
  provinces: string[]
  flag_name?: string
  population?: number
}

export interface Government {
  party?: Party
}

export interface Party {
  party_name?: string
  leader?: string
  leader_portrait_path?: string
}

export interface Event {
  id: string,
  occur_date: string
  occur_country_code: string
  title: string
  text: string
  button_text: string
  effects?: Effect[]
}

export interface Effect{
  key: string
  value: string
}
