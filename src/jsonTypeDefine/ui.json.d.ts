export interface UIData {
    color: Color
  }
  
  export interface Color {
    ui_container: string
    map: Map
  }
  
  export interface Map {
    ocean: string
    field: string
    border: string
    fill_color_opacity: string
  }