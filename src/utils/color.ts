export class ColorUtils
{
    public static getRandomColorStr(): string
    {
        var color = Math.floor(Math.random() * 16777215).toString(16);
        var i = 0;

        for (i = color.length; i < 6; i++)
            color = "0" + color;

        return "#" + color;
    }
}