import { GameEvent } from "../game/events/gameEvent";
import { Country } from "../game/histories/country";

export class Scenario
{
    public title: string;
    public countries: Array<Country>;
    public events: Array<GameEvent>;
    public worldClock: Date;

    constructor(title: string,
                countries: Array<Country>,
                events: Array<GameEvent>,
                worldClock: Date)
    {
        this.title = title;
        this.countries = countries;
        this.events = events;
        this.worldClock = worldClock;
    }
}