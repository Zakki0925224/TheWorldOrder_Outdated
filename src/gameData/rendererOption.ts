export class RendererOption
{
    public uiContainerColor: string;
    public mapOceanColor: string;
    public mapFieldColor: string;
    public mapBorderColor: string;
    public mapFillColorOpacity: string;

    constructor(uiContainerColor: string,
                mapOceanColor: string,
                mapFieldColor: string,
                mapBorderColor: string,
                mapFillColorOpacity: string)
    {
        this.uiContainerColor = uiContainerColor;
        this.mapOceanColor = mapOceanColor;
        this.mapFieldColor = mapFieldColor;
        this.mapBorderColor = mapBorderColor;
        this.mapFillColorOpacity = mapFillColorOpacity;
    }
}