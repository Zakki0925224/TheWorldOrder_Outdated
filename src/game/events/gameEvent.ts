import { EffectInfo } from "../players/effect";

export class GameEvent
{
    public id: string;
    public occurDate: Date;
    public occurCountryCode: string;
    public title: string;
    public text: string;
    public buttonText: string;
    public effects: Array<EffectInfo>;

    constructor(id: string,
                occurDate: Date,
                occurCountryCode: string,
                title: string,
                text: string,
                buttonText: string,
                effects: Array<EffectInfo>)
    {
        this.id = id;
        this.occurDate = occurDate;
        this.occurCountryCode = occurCountryCode;
        this.title = title;
        this.text = text;
        this.buttonText = buttonText;
        this.effects = effects;
    }
}