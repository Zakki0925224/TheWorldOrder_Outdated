export enum Effects
{
    // instant
    NATIONAL_HAPPINESS,
    NATIONAL_STABILITY,
    POPULATION,
    ECONOMY_POWER,
    MONEY,

    // monthly
    MONTHLY_NATIONAL_HAPPINESS,
    MONTHLY_NATIONAL_STABILITY,
    MONTHLY_POPULATION,
    MONTHLY_ECONOMY_POWER,
    MONTHLY_MONEY,

    // other
    SET_COUNTRY_NAME,
    SET_COUNTRY_FLAG,
    SET_LEADER_NAME,
    SET_LEADER_PORTRAIT,
    SET_PARTY_NAME
}

export class EffectInfo
{
    public effect: Effects;
    public value: string;

    constructor(effect: Effects, value: string)
    {
        this.effect = effect;
        this.value = value;
    }
}