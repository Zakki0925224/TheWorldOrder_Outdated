import { GameEvent } from "../events/gameEvent";
import { Country } from "../histories/country";
import { Buildings } from "../histories/province";
import { Effects, EffectInfo } from "./effect";
import { IPlayer } from "./iPlayer";

export class AI implements IPlayer
{
    country: Country;
    events: Array<GameEvent>;
    monthlyEffectInfo: Array<EffectInfo>;

    constructor(country: Country, events: Array<GameEvent>)
    {
        this.country = country;
        this.events = events;
        this.monthlyEffectInfo = new Array<EffectInfo>();
    }

    monthlyUpdate(date: Date): void
    {
        this.monthlyEffectInfo.forEach((v) => this.executeEffect(v.effect, v.value));
        this.country.setMonthlyBudget();

        this.country.getProvinces().forEach((p) =>
        {
            for (var i = 0; i <= p.getInfrastructureLevel(); i++) this.country.useMoney(Buildings.getMonthlyCost(Buildings.INFRASTRUCTURE) * i);
            for (var i = 0; i <= p.getResidenceLevel(); i++) this.country.useMoney(Buildings.getMonthlyCost(Buildings.RESIDENCE) * i);
            for (var i = 0; i <= p.getFactoryLevel(); i++) this.country.useMoney(Buildings.getMonthlyCost(Buildings.FACTORY) * i);
            for (var i = 0; i <= p.getPoliceStationLevel(); i++) this.country.useMoney(Buildings.getMonthlyCost(Buildings.POLICE_STATION) * i);
            for (var i = 0; i <= p.getHospitalLevel(); i++) this.country.useMoney(Buildings.getMonthlyCost(Buildings.HOSPITAL) * i);
        });

        this.country.addPopulation(40000);
    }

    dailyUpdate(date: Date): void
    {
        this.country.getProvinces().forEach((v) => v.dailyUpdate(date));

        this.events.forEach((v) =>
        {
            if (v.occurDate.getUTCFullYear() == date.getUTCFullYear() &&
                v.occurDate.getUTCMonth() == date.getUTCMonth() &&
                v.occurDate.getUTCDate() == date.getUTCDate())
                console.log(v);
        });
    }

    executeEffect(effect: Effects, value: string): void
    {
        var val = 0;
        try
        {
            val = parseInt(value);
        }
        catch (e)
        {
            console.log(e.message);
        }

        if (effect == Effects.NATIONAL_HAPPINESS ||
            effect == Effects.MONTHLY_NATIONAL_HAPPINESS)
            this.country.addNationalHappiness(val);

        else if (effect == Effects.NATIONAL_STABILITY ||
                 effect == Effects.MONTHLY_NATIONAL_STABILITY)
            this.country.addNationalStability(val);

        else if (effect == Effects.POPULATION ||
                 effect == Effects.MONTHLY_POPULATION)
            this.country.addPopulation(val);

        else if (effect == Effects.ECONOMY_POWER ||
                 effect == Effects.MONTHLY_ECONOMY_POWER)
            this.country.addEconomyPower(val);

        else if (effect == Effects.MONEY ||
                 effect == Effects.MONTHLY_MONEY)
            this.country.addMoney(val);

        else if (effect == Effects.SET_COUNTRY_NAME)
            this.country.setName(value);

        else if (effect == Effects.SET_COUNTRY_FLAG)
            this.country.setFlagName(value);

        else if (effect == Effects.SET_LEADER_NAME)
            this.country.getGovernment().party.leaderName = value;

        else if (effect == Effects.SET_LEADER_PORTRAIT)
            this.country.getGovernment().party.leaderPortraitName = value;

        else if (effect == Effects.SET_PARTY_NAME)
            this.country.getGovernment().party.name = value;
    }

    registerEffect(effect: Effects, value: string): void
    {
        this.monthlyEffectInfo.push(new EffectInfo(effect, value));
    }
}