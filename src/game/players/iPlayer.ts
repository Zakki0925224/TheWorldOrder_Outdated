import { GameEvent } from "../events/gameEvent";
import { Country } from "../histories/country";
import { Effects, EffectInfo } from "./effect";

export interface IPlayer
{
    country: Country;
    events: Array<GameEvent>;
    monthlyEffectInfo: Array<EffectInfo>;

    monthlyUpdate(date: Date): void;
    dailyUpdate(date: Date): void;
    executeEffect(effect: Effects, value: string): void;
    registerEffect(effect: Effects, value: string): void;
}