import { Game } from "../game";

export class TitleRenderer
{
    private game: Game;
    private flagPath: string = "../assets/images/flag";
    private selectWindowContainer: HTMLElement = document.getElementById("select-window-container") as HTMLElement;
    private scenarioSelector: HTMLSelectElement = document.getElementById("scenario-selector") as HTMLSelectElement;
    private countrySelector: HTMLSelectElement = document.getElementById("country-selector") as HTMLSelectElement;

    constructor(game: Game)
    {
        this.game = game;
    }

    public initialize(): void
    {
        this.selectWindowContainer.style.display = "none";
        this.addEvents();
        this.generateSelectWindow();
    }

    private addEvents(): void
    {
        document.getElementById("start-button")?.addEventListener("click", (event: Event) => this.openSelectWindow(event));
        document.getElementById("game-start-button")?.addEventListener("click", (event: Event) => this.startGame(event));
        document.getElementById("cancel-button")?.addEventListener("click", (event: Event) => this.closeSelectWindow(event));
    }

    private generateSelectWindow(): void
    {
        for (var i = 0; i < this.game.scenarios.length; i++)
        {
            var item = document.createElement("option");
            item.setAttribute("value", this.game.scenarios[i].title);
            item.innerHTML = this.game.scenarios[i].title;
            this.scenarioSelector.appendChild(item);
        }

        (document.getElementById("start-date-label") as HTMLElement).innerHTML = this.game.scenarios[this.scenarioSelector.selectedIndex].worldClock.toISOString();
        this.drawCountrySelector(this.scenarioSelector.selectedIndex);

        this.selectWindowContainer.style.display = "none";
    }

    private openSelectWindow(event: Event): void
    {
        this.selectWindowContainer.style.display = "block";
    }

    private closeSelectWindow(event: Event): void
    {
        this.selectWindowContainer.style.display = "none";
    }

    private startGame(event: Event): void
    {
        var scenarioID = this.scenarioSelector.selectedIndex;
        var playerCountryCode = this.game.scenarios[scenarioID].countries[this.countrySelector.selectedIndex].getCode();
        window.sessionStorage.setItem("scenarioID", scenarioID.toString());
        window.sessionStorage.setItem("playerCountryCode", playerCountryCode);
        location.replace("./game.html");


        if (this.game.isInitialized)
            location.replace("./game.html");
    }

    private drawCountrySelector(scenarioID: number): void
    {
        var countries = this.game.scenarios[scenarioID].countries;
        this.countrySelector.setAttribute("size", "5");

        for (var i = 0; i < countries.length; i++)
        {
            var img = document.createElement("img");
            img.src = this.flagPath + "/" + countries[i].getFlagName();
            img.height = 64;
            img.width = 96;

            var p = document.createElement("p");
            p.innerHTML = countries[i].getName();

            var item = document.createElement("option");
            item.style.border = "solid 1px";
            item.style.margin = "5px";
            item.appendChild(img);
            item.appendChild(p);
            this.countrySelector.appendChild(item);
        }
    }
}