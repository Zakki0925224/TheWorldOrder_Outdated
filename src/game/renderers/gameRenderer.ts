import SvgPanZoom from "svg-pan-zoom";
import { GameEvent } from "../events/gameEvent";
import { Country } from "../histories/country";
import { Game } from "../game";
import { RendererOption } from "../../gameData/rendererOption";
import { Buildings, Province } from "../histories/province";

export class GameRenderer
{
    private map: HTMLObjectElement | null = null;
    private mapNodes: Array<ChildNode> = new Array<ChildNode>();
    private provinceNodes: Array<ChildNode> = new Array<ChildNode>();
    private panZoom!: SvgPanZoom.Instance;

    private game: Game;
    private rendererOption: RendererOption;
    private imagePath: string = "../assets/images";
    private intervalTimer!: NodeJS.Timer;
    private isPlayingTimer: boolean = false;
    private timerSpeed: number = 10;
    private isShowingGovernmentWindow: boolean = false;
    private isShowingEconomyWindow: boolean = false;
    private isShowingConstructionWindow: boolean = false;
    private isShowingTradeWindow: boolean = false;
    private isShowingLankingWindow: boolean = false;
    private isShowingProvinceWindow: boolean = false;

    private mapContainer: HTMLElement = document.getElementById("map-container") as HTMLElement;

    private clockDisplayButton: HTMLButtonElement = document.getElementById("clock-display-button") as HTMLButtonElement;
    private governmentWindowContainer: HTMLElement = document.getElementById("government-window-container") as HTMLElement;
    private economyWindowContainer: HTMLElement = document.getElementById("economy-window-container") as HTMLElement;
    private constructionWindowContainer: HTMLElement = document.getElementById("construction-window-container") as HTMLElement;
    private tradeWindowContainer: HTMLElement = document.getElementById("trade-window-container") as HTMLElement;
    private lankingWindowContainer: HTMLElement = document.getElementById("lanking-window-container") as HTMLElement;
    private provinceWindowContainer: HTMLElement = document.getElementById("province-window-container") as HTMLElement;

    private showingProvince: Province | null = null;

    private provinceWindowInfrastructureBuildButton: HTMLButtonElement = document.getElementById("province-window-infrastructure-build-button") as HTMLButtonElement;
    private provinceWindowResidenceBuildButton: HTMLButtonElement = document.getElementById("province-window-residence-build-button") as HTMLButtonElement;
    private provinceWindowFactoryBuildButton: HTMLButtonElement = document.getElementById("province-window-factory-build-button") as HTMLButtonElement;
    private provinceWindowPoliceStationBuildButton: HTMLButtonElement = document.getElementById("province-window-police-station-build-button") as HTMLButtonElement;
    private provinceWindowHospitalBuildButton: HTMLButtonElement = document.getElementById("province-window-hospital-build-button") as HTMLButtonElement;

    private taxRateSlider = document.getElementById("tax-rate-slider") as HTMLInputElement;

    constructor(game: Game)
    {
        this.game = game;
        this.rendererOption = this.game.rendererOption;
    }

    public initialize(): void
    {
        this.game.initializeGame(Number.parseInt(window.sessionStorage.getItem("scenarioID")!), window.sessionStorage.getItem("playerCountryCode")!);
        this.initializeControls();
        this.initializeMap();

        console.log(this.game.player?.country.getCode());
    }

    private initializeControls(): void
    {
        this.clockDisplayButton.innerText = this.game.worldClock.toISOString();
        this.clockDisplayButton.addEventListener("click", () =>
        {
            if (this.isPlayingTimer)
                this.stopTimer();

            else
                this.startTimer();
        });

        document.getElementById("accel-button")?.addEventListener("click", () =>
        {
            if (this.timerSpeed > 51)
                this.timerSpeed -= 50;

            if (this.isPlayingTimer)
            {
                this.stopTimer();
                this.startTimer();
            }

            console.log("timer speed: " + this.timerSpeed);
        });

        document.getElementById("deaccel-button")?.addEventListener("click", () =>
        {
            if (this.timerSpeed < 200)
                this.timerSpeed += 50;

            if (this.isPlayingTimer)
            {
                this.stopTimer();
                this.startTimer();
            }

            console.log("timer speed: " + this.timerSpeed);
        });

        var img = document.createElement("img");
        img.src = this.imagePath + "/flag/" + this.game.player?.country.getFlagName();
        img.width = 60;
        img.height = 40;

        document.getElementById("flag-container")?.appendChild(img);

        (document.getElementById("government-button") as HTMLButtonElement).addEventListener("click", () =>
        {
            if (this.isShowingGovernmentWindow)
            {
                this.governmentWindowContainer.style.display = "none";
                this.isShowingGovernmentWindow = false;
            }
            else
            {
                var country = this.game.player?.country;

                if (country)
                {
                    this.showGovernmentSubWindow(country);
                    this.governmentWindowContainer.style.display = "block";
                    this.isShowingGovernmentWindow = true;
                }
            }
        });

        (document.getElementById("economy-button") as HTMLButtonElement).addEventListener("click", () =>
        {
            if (this.isShowingEconomyWindow)
            {
                this.economyWindowContainer.style.display = "none";
                this.isShowingEconomyWindow = false;
            }
            else
                this.showEconomySubWindow();
        });

        (document.getElementById("construction-button") as HTMLButtonElement).addEventListener("click", () =>
        {
            if (this.isShowingConstructionWindow)
            {
                this.constructionWindowContainer.style.display = "none";
                this.isShowingConstructionWindow = false;
            }
            else
                this.showConstructionSubWindow();
        });

        (document.getElementById("trade-button") as HTMLButtonElement).addEventListener("click", () =>
        {
            if (this.isShowingTradeWindow)
            {
                this.tradeWindowContainer.style.display = "none";
                this.isShowingTradeWindow = false;
            }
            else
                this.showTradeSubWindow();
        });

        (document.getElementById("lanking-button") as HTMLButtonElement).addEventListener("click", () =>
        {
            if (this.isShowingLankingWindow)
            {
                this.lankingWindowContainer.style.display = "none";
                this.isShowingLankingWindow = false;
            }
            else
                this.showLankingSubWindow();
        });

        this.governmentWindowContainer.style.display =
        this.economyWindowContainer.style.display =
        this.constructionWindowContainer.style.display =
        this.tradeWindowContainer.style.display =
        this.lankingWindowContainer.style.display = "none";

        (document.getElementById("province-window-close-button") as HTMLButtonElement).addEventListener("click", () =>
        {
            this.provinceWindowContainer.style.display = "none";
            this.isShowingProvinceWindow = false;
            this.showingProvince = null;
        });

        this.provinceWindowInfrastructureBuildButton.addEventListener("click", () =>
        {
            if (this.showingProvince)
            {
                this.showingProvince.build(Buildings.INFRASTRUCTURE, this.game.worldClock);
                this.game.player?.country.useMoney(Buildings.getBuildCost(Buildings.INFRASTRUCTURE));
                this.provinceWindowInfrastructureBuildButton.disabled = true;
            }
        });

        this.provinceWindowResidenceBuildButton.addEventListener("click", () =>
        {
            if (this.showingProvince)
            {
                this.showingProvince.build(Buildings.RESIDENCE, this.game.worldClock);
                this.game.player?.country.useMoney(Buildings.getBuildCost(Buildings.RESIDENCE));
                this.provinceWindowResidenceBuildButton.disabled = true;
            }
        });

        this.provinceWindowFactoryBuildButton.addEventListener("click", () =>
        {
            if (this.showingProvince)
            {
                this.showingProvince.build(Buildings.FACTORY, this.game.worldClock);
                this.game.player?.country.useMoney(Buildings.getBuildCost(Buildings.FACTORY));
                this.provinceWindowFactoryBuildButton.disabled = true;
            }
        });

        this.provinceWindowPoliceStationBuildButton.addEventListener("click", () =>
        {
            if (this.showingProvince)
            {
                this.showingProvince.build(Buildings.POLICE_STATION, this.game.worldClock);
                this.game.player?.country.useMoney(Buildings.getBuildCost(Buildings.POLICE_STATION));
                this.provinceWindowPoliceStationBuildButton.disabled = true;
            }
        });

        this.provinceWindowHospitalBuildButton.addEventListener("click", () =>
        {
            if (this.showingProvince)
            {
                this.showingProvince.build(Buildings.HOSPITAL, this.game.worldClock);
                this.game.player?.country.useMoney(Buildings.getBuildCost(Buildings.HOSPITAL));
                this.provinceWindowHospitalBuildButton.disabled = true;
            }
        });

        var player = this.game.player;

        this.taxRateSlider.addEventListener("input", () =>
        {
            var value = parseInt(this.taxRateSlider.value);
            (document.getElementById("tax-rate-label") as HTMLParagraphElement).innerText = "Add " + value + "% of Money every month, NationHappiness: " + (-value) + "%";

            if (player)
            {
                player.country.setTaxRate(value / 100);
            }
        });

        this.provinceWindowContainer.style.display = "none";

        this.updateInfoPanel();
    }

    private initializeMap(): void
    {
        this.map = document.createElement("object");
        this.map.setAttribute("id", "map");
        this.map.setAttribute("type", "image/svg+xml");
        this.map.setAttribute("data", this.imagePath + "/map/world-states-provinces.svg");

        this.mapContainer.appendChild(this.map);

        this.map.addEventListener("load", async() =>
        {
            let promise = new Promise(async resolve =>
            {
                let result = await promise;

                // Map initialize
                //svg pan zoom
                this.panZoom = SvgPanZoom(this.map!, { minZoom: 2, maxZoom: 800, dblClickZoomEnabled: false });
                this.panZoom.pan({x: 50, y: 50});

                this.getMapNodes(); // loaded timing is best
                this.drawColor();
                this.setMapEvents();
            });
        });
    }

    private getMapNodes(): void
    {
        // get nodes
        this.map!.getSVGDocument()!.childNodes[1].childNodes[1].childNodes.forEach((node) =>
        {
            if (node.nodeType == 1)
            {
                this.mapNodes!.push(node);
            }
        });

        // get province nodes
        this.mapNodes.forEach((node) =>
        {
            node.childNodes.forEach((_node) =>
            {
                if ((_node as HTMLElement).tagName == "path")
                    this.provinceNodes.push(_node);
            });
        });
    }

    private drawColor(): void
    {
        // out color
        (this.mapNodes[0] as HTMLElement).style.fill = this.rendererOption.uiContainerColor;
        // svg ocean color
        (this.mapNodes[1] as HTMLElement).style.fill = this.rendererOption.mapOceanColor;

        this.drawBorderColor();
        this.drawFieldColor();
        this.drawCountryColor();
    }

    private drawBorderColor(): void
    {
        this.provinceNodes.forEach((province) => (province as HTMLElement).style.stroke = this.rendererOption.mapBorderColor);
    }

    private drawFieldColor(): void
    {
        this.provinceNodes.forEach((province) => (province as HTMLElement).style.fill = this.rendererOption.mapFieldColor);
    }

    private drawCountryColor(): void
    {
        this.game.scenarios[this.game.scenarioID].countries.forEach((c) =>
        {
            c.getProvinces().forEach((province) =>
            {
                this.provinceNodes.forEach((p) =>
                {
                    if (province.getId() == (p as HTMLElement).id)
                    {
                        (p as HTMLElement).style.fill = c.getColorStr();
                        (p as HTMLElement).style.fillOpacity = this.rendererOption.mapFillColorOpacity;
                    }
                });
            });
        });
    }

    private setMapEvents(): void
    {
        // province click event
        this.provinceNodes.forEach((province) => (province as HTMLElement).addEventListener("click", (e: Event) => this.provinceOnClicked((province as HTMLElement).id, e)));

    }

    private provinceOnClicked(id: string, e: Event): void
    {
        var player = this.game.player;

        this.game.scenarios[this.game.scenarioID].countries.forEach((country) =>
        {
            country.getProvinces().forEach((province) =>
            {
                if (province.getId() == id && country.getCode() != player?.country.getCode())
                {
                    console.log(id + ", " + country.getName());
                    this.showGovernmentSubWindow(country);
                }
            });
        });

        player?.country.getProvinces().forEach((v) =>
        {
            if (id == v.getId())
            {
                this.showingProvince = v;
                this.showProvinceWindow();
            }
        });
    }

    private showGovernmentSubWindow(country: Country): void
    {
        var playerCountryCode = this.game.player?.country.getCode();

        var flagImage = document.getElementById("flag-image") as HTMLImageElement;
        var countryNameText = document.getElementById("country-name-text") as HTMLParagraphElement;
        var leaderPortrait = document.getElementById("leader-portrait") as HTMLImageElement;
        var leaderNameText = document.getElementById("leader-name-text") as HTMLParagraphElement;
        var partyNameText = document.getElementById("party-name-text") as HTMLParagraphElement;

        var party = country.getGovernment().party;
        var partyName = "---";
        var leaderName = "---";

        if (party)
        {
            partyName = party.name;
            leaderName = party.leaderName;
        }

        flagImage.src = this.imagePath + "/flag/" + country.getFlagName();
        flagImage.width = 120;
        flagImage.height = 80;
        countryNameText.innerText = country.getName();
        leaderPortrait.src = this.imagePath + "/portrait/" + party.leaderPortraitName;
        leaderPortrait.width = 80;
        leaderPortrait.height = 120;
        leaderNameText.innerText = leaderName;
        partyNameText.innerText = partyName;

        var disableCheck;

        if (country.getCode() != playerCountryCode)
            disableCheck = true;

        else
            disableCheck = false;

        // diplomacy
        (document.getElementById("declare-war-button") as HTMLButtonElement).disabled =
        (document.getElementById("military-alliance-button") as HTMLButtonElement).disabled =
        (document.getElementById("prise-the-government-button") as HTMLButtonElement).disabled =
        (document.getElementById("criticize-the-government-button") as HTMLButtonElement).disabled =
        (document.getElementById("request-a-puppet-button") as HTMLButtonElement).disabled =
        (document.getElementById("economy-support-button") as HTMLButtonElement).disabled = !disableCheck;

        this.governmentWindowContainer.style.display = "block";
        this.isShowingGovernmentWindow = true;

        this.economyWindowContainer.style.display =
        this.constructionWindowContainer.style.display =
        this.tradeWindowContainer.style.display =
        this.lankingWindowContainer.style.display = "none";

        this.isShowingEconomyWindow =
        this.isShowingConstructionWindow =
        this.isShowingTradeWindow =
        this.isShowingLankingWindow = false;
    }

    private showEconomySubWindow(): void
    {
        this.economyWindowContainer.style.display = "block";
        this.isShowingEconomyWindow = true;

        this.governmentWindowContainer.style.display =
        this.constructionWindowContainer.style.display =
        this.tradeWindowContainer.style.display =
        this.lankingWindowContainer.style.display = "none";

        this.isShowingGovernmentWindow =
        this.isShowingConstructionWindow =
        this.isShowingTradeWindow =
        this.isShowingLankingWindow = false;
    }

    private showConstructionSubWindow(): void
    {
        this.updateConstructionSubWindow();

        this.constructionWindowContainer.style.display = "block";
        this.isShowingConstructionWindow = true;

        this.governmentWindowContainer.style.display =
        this.economyWindowContainer.style.display =
        this.tradeWindowContainer.style.display =
        this.lankingWindowContainer.style.display = "none";

        this.isShowingGovernmentWindow =
        this.isShowingEconomyWindow =
        this.isShowingTradeWindow =
        this.isShowingLankingWindow = false;
    }

    private showTradeSubWindow(): void
    {
        this.tradeWindowContainer.style.display = "block";
        this.isShowingTradeWindow = true;

        this.governmentWindowContainer.style.display =
        this.economyWindowContainer.style.display =
        this.constructionWindowContainer.style.display =
        this.lankingWindowContainer.style.display = "none";

        this.isShowingGovernmentWindow =
        this.isShowingEconomyWindow =
        this.isShowingConstructionWindow =
        this.isShowingLankingWindow = false;
    }

    private showLankingSubWindow(): void
    {
        var playerCountry = this.game.player?.country;
        var aiCountries = new Array<Country>();

        this.game.AIs.forEach((v) => aiCountries.push(v.country));

        var table = document.getElementById("lanking-window-table");
        var tr1 = document.createElement("tr");
        var header1 = document.createElement("th");
        header1.innerText = "Country";
        var header2 = document.createElement("th");
        header2.innerText = "Money";
        var header3 = document.createElement("th");
        header3.innerText = "EconomyPower";
        var header4 = document.createElement("th");
        header4.innerText = "Population";

        tr1.appendChild(header1);
        tr1.appendChild(header2);
        tr1.appendChild(header3);
        tr1.appendChild(header4);

        if (table)
        {
            while (table.firstChild) table.removeChild(table.firstChild);
            table.appendChild(tr1);

            var tr2 = document.createElement("tr");

            if (playerCountry)
            {
                var td1 = document.createElement("td");
                var td2 = document.createElement("td");
                var td3 = document.createElement("td");
                var td4 = document.createElement("td");
                td2.setAttribute("align", "right");
                td3.setAttribute("align", "right");
                td4.setAttribute("align", "right");
                td1.innerText = playerCountry.getName();
                td2.innerText = Math.round(playerCountry.getMoney()).toString();
                td3.innerText = Math.round(playerCountry.getEconomyPower()).toString();
                td4.innerText = playerCountry.getPopulation().toString();
                tr2.appendChild(td1);
                tr2.appendChild(td2);
                tr2.appendChild(td3);
                tr2.appendChild(td4);
            }

            table.appendChild(tr2);

            aiCountries.forEach((v) =>
            {
                var tr = document.createElement("tr");
                var td1 = document.createElement("td");
                var td2 = document.createElement("td");
                var td3 = document.createElement("td");
                var td4 = document.createElement("td");
                td2.setAttribute("align", "right");
                td3.setAttribute("align", "right");
                td4.setAttribute("align", "right");
                td1.innerText = v.getName();
                td2.innerText = v.getMoney().toString();
                td3.innerText = v.getEconomyPower().toString();
                td4.innerText = v.getPopulation().toString();
                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);
                tr.appendChild(td4);

                table?.appendChild(tr);
            });

            this.lankingWindowContainer.appendChild(table);
        }

        this.lankingWindowContainer.style.display = "block";
        this.isShowingLankingWindow = true;

        this.governmentWindowContainer.style.display =
        this.economyWindowContainer.style.display =
        this.constructionWindowContainer.style.display =
        this.tradeWindowContainer.style.display = "none";

        this.isShowingGovernmentWindow =
        this.isShowingEconomyWindow =
        this.isShowingConstructionWindow =
        this.isShowingTradeWindow = false;
    }

    private showProvinceWindow(): void
    {
        if (this.showingProvince)
        {
            this.updateProvinceWindow();
            (document.getElementById("province-window-province-name-label") as HTMLHeadElement).innerText = this.showingProvince.getName();
            this.provinceWindowContainer.style.display = "block";
            this.isShowingProvinceWindow = true;
        }
    }

    private generateEventWindow(event: GameEvent)
    {
        var container = document.createElement("div");
        container.id = "event-window-container";
        container.className = "window-container";

        var table = document.createElement("table");
        var title = document.createElement("h3");
        title.innerText = event.title;
        var message = document.createElement("p");
        message.innerText = event.text;
        var buttons = document.createElement("div");

        var button = document.createElement("button");
        button.className = "button-ui";
        button.innerText = event.buttonText;
        button.addEventListener("click", () => this.mapContainer.removeChild(container));

        buttons.appendChild(button);

        var tr1 = document.createElement("tr");
        tr1.appendChild(title);
        var tr2 = document.createElement("tr");
        tr2.appendChild(message);
        var tr3 = document.createElement("tr");
        tr3.appendChild(buttons);
        table.appendChild(tr1);
        table.appendChild(tr2);
        table.appendChild(tr3);
        container.appendChild(table);

        this.mapContainer.prepend(container);
    }

    private startTimer(): void
    {
        this.intervalTimer = setInterval(() => this.intervalProcess(), this.timerSpeed);
        this.isPlayingTimer = true;
        console.log("timer started, speed: " + this.timerSpeed);
    }

    private stopTimer(): void
    {
        clearInterval(this.intervalTimer);
        this.isPlayingTimer = false;
        console.log("timer stopped");
    }

    private intervalProcess(): void
    {
        var worldClock = this.game.worldClock;

        var oldMonth = worldClock.getUTCMonth();
        var oldDate = worldClock.getUTCDate();

        worldClock.setUTCHours(worldClock.getUTCHours() + 1);
        this.clockDisplayButton.innerText = worldClock.toISOString();
        this.updateInfoPanel();

        // after 1 month
        if (worldClock.getMonth() != oldMonth && worldClock.getUTCHours() == 0)
        {
            this.game.player?.monthlyUpdate(worldClock);
            this.game.AIs.forEach((v) => v.monthlyUpdate(worldClock));
        }

        // after 1 day
        if (worldClock.getUTCDate() != oldDate)
        {
            var event = this.game.player?.dailyUpdate(worldClock);
            this.game.AIs.forEach((v) => v.dailyUpdate(worldClock));

            if (event != null)
                this.generateEventWindow(event);

            if (this.isShowingProvinceWindow)
                this.updateProvinceWindow();
        }

        if (this.isShowingConstructionWindow)
            this.updateConstructionSubWindow();
    }

    private updateInfoPanel(): void
    {
        var playerCountry = this.game.player?.country;
        var money = playerCountry?.getMoney();
        var economyPower = playerCountry?.getEconomyPower();
        var population = playerCountry?.getPopulation();
        var nationalStability = playerCountry?.getNationalStability();
        var nationalHappiness = playerCountry?.getNationalHappiness();

        if (money != undefined) (document.getElementById("money-panel-label") as HTMLParagraphElement).innerText = "Money: " + Math.round(money);
        if (economyPower != undefined) (document.getElementById("economy-power-panel-label") as HTMLParagraphElement).innerText = "EconomyPower: " + Math.round(economyPower);
        if (population != undefined) (document.getElementById("population-panel-label") as HTMLParagraphElement).innerText = "Population: " + population;
        if (nationalStability != undefined) (document.getElementById("national-stability-panel-label") as HTMLParagraphElement).innerText = "NationStability: " + Math.round(nationalStability * 100) + "%";
        if (nationalHappiness != undefined) (document.getElementById("national-happiness-panel-label") as HTMLParagraphElement).innerText = "NationHappiness: " + Math.round(nationalHappiness * 100) + "%";
    }

    private updateProvinceWindow(): void
    {
        if (this.showingProvince)
        {
            (document.getElementById("province-window-infrastructure-level-label") as HTMLParagraphElement).innerText = this.showingProvince.getInfrastructureLevel() + "/" + this.showingProvince.getMaxLevel();
            (document.getElementById("province-window-residence-level-label") as HTMLParagraphElement).innerText = this.showingProvince.getResidenceLevel() + "/" + this.showingProvince.getMaxLevel();
            (document.getElementById("province-window-factory-level-label") as HTMLParagraphElement).innerText = this.showingProvince.getFactoryLevel() + "/" + this.showingProvince.getMaxLevel();
            (document.getElementById("province-window-police-station-level-label") as HTMLParagraphElement).innerText = this.showingProvince.getPoliceStationLevel() + "/" + this.showingProvince.getMaxLevel();
            (document.getElementById("province-window-hospital-level-label") as HTMLParagraphElement).innerText = this.showingProvince.getHospitalLevel() + "/" + this.showingProvince.getMaxLevel();

            var wc = this.game.worldClock;
            var infrastructureBuildingRate = this.showingProvince.getBuildingRate(Buildings.INFRASTRUCTURE, wc);
            var residenceBuildingRate = this.showingProvince.getBuildingRate(Buildings.RESIDENCE, wc);
            var factoryBuildingRate = this.showingProvince.getBuildingRate(Buildings.FACTORY, wc);
            var policeStationBuildingRate = this.showingProvince.getBuildingRate(Buildings.POLICE_STATION, wc);
            var hospitalBuildingRate = this.showingProvince.getBuildingRate(Buildings.HOSPITAL, wc);
            var infrastructureBuildingRateLabel = document.getElementById("province-window-infrastructure-building-label") as HTMLParagraphElement;
            var residenceBuildingRateLabel = document.getElementById("province-window-residence-building-label") as HTMLParagraphElement;
            var factoryBuildingRateLabel = document.getElementById("province-window-factory-building-label") as HTMLParagraphElement;
            var policeStationBuildingRateLabel = document.getElementById("province-window-police-station-building-label") as HTMLParagraphElement;
            var hospitalBuildingRateLabel = document.getElementById("province-window-hospital-building-label") as HTMLParagraphElement;

            if (infrastructureBuildingRate != -1)
            {
                this.provinceWindowInfrastructureBuildButton.disabled = true;
                infrastructureBuildingRateLabel.innerText = "Building: " + Math.round(infrastructureBuildingRate * 100) + "%";
            }
            else
            {
                this.provinceWindowInfrastructureBuildButton.disabled = false;
                infrastructureBuildingRateLabel.innerText = "Build cost: " + Buildings.getBuildCost(Buildings.INFRASTRUCTURE) + ", \nMonthly cost: " + Buildings.getMonthlyCost(Buildings.INFRASTRUCTURE);
            }

            if (residenceBuildingRate != -1)
            {
                this.provinceWindowResidenceBuildButton.disabled = true;
                residenceBuildingRateLabel.innerText = "Building: " + Math.round(residenceBuildingRate * 100) + "%";
            }
            else
            {
                this.provinceWindowResidenceBuildButton.disabled = false;
                residenceBuildingRateLabel.innerText = "Build cost: " + Buildings.getBuildCost(Buildings.RESIDENCE) + ", \nMonthly cost: " + Buildings.getMonthlyCost(Buildings.RESIDENCE);
            }

            if (factoryBuildingRate != -1)
            {
                this.provinceWindowFactoryBuildButton.disabled = true;
                factoryBuildingRateLabel.innerText = "Building: " + Math.round(factoryBuildingRate * 100) + "%";
            }
            else
            {
                this.provinceWindowFactoryBuildButton.disabled = false;
                factoryBuildingRateLabel.innerText = "Build cost: " + Buildings.getBuildCost(Buildings.FACTORY) + ", \nMonthly cost: " + Buildings.getMonthlyCost(Buildings.FACTORY);
            }

            if (policeStationBuildingRate != -1)
            {
                this.provinceWindowPoliceStationBuildButton.disabled = true;
                policeStationBuildingRateLabel.innerText = "Building: " + Math.round(policeStationBuildingRate * 100) + "%";
            }
            else
            {
                this.provinceWindowPoliceStationBuildButton.disabled = false;
                policeStationBuildingRateLabel.innerText = "Build cost: " + Buildings.getBuildCost(Buildings.POLICE_STATION) + ", \nMonthly cost: " + Buildings.getMonthlyCost(Buildings.POLICE_STATION);
            }

            if (hospitalBuildingRate != -1)
            {
                this.provinceWindowHospitalBuildButton.disabled = true;
                hospitalBuildingRateLabel.innerText = "Building: " + Math.round(hospitalBuildingRate * 100) + "%";
            }
            else
            {
                this.provinceWindowHospitalBuildButton.disabled = false;
                hospitalBuildingRateLabel.innerText = "Build cost: " + Buildings.getBuildCost(Buildings.HOSPITAL) + ", \nMonthly cost: " + Buildings.getMonthlyCost(Buildings.HOSPITAL);
            }

            if (this.showingProvince.getInfrastructureLevel() == this.showingProvince.getMaxLevel())
                this.provinceWindowInfrastructureBuildButton.disabled = true;

            if (this.showingProvince.getResidenceLevel() == this.showingProvince.getMaxLevel())
                this.provinceWindowResidenceBuildButton.disabled = true;

            if (this.showingProvince.getFactoryLevel() == this.showingProvince.getMaxLevel())
                this.provinceWindowFactoryBuildButton.disabled = true;

            if (this.showingProvince.getPoliceStationLevel() == this.showingProvince.getMaxLevel())
                this.provinceWindowPoliceStationBuildButton.disabled = true;

            if (this.showingProvince.getHospitalLevel() == this.showingProvince.getMaxLevel())
                this.provinceWindowHospitalBuildButton.disabled = true;
        }
    }

    private updateConstructionSubWindow(): void
    {
        var table = (document.getElementById("construction-window-table") as HTMLTableElement);
        while (table.firstChild) table.removeChild(table.firstChild);
        var trh = document.createElement("tr");
        var th1 = document.createElement("th");
        var th2 = document.createElement("th");
        var th3 = document.createElement("th");
        th1.innerText = "Province";
        th2.innerText = "Building";
        th3.innerText = "Rate";
        trh.appendChild(th1);
        trh.appendChild(th2);
        trh.appendChild(th3);
        table.appendChild(trh);

        this.game.player?.country.getProvinces().forEach((p) =>
        {
            p.getBuildingInfo().forEach((i) =>
            {
                var tr = document.createElement("tr");
                var td1 = document.createElement("td");
                var td2 = document.createElement("td");
                var td3 = document.createElement("td");
                td1.innerText = p.getName();
                td2.innerText = Buildings.toString(i.getBuilding());
                td3.innerText = Math.round(i.getBuildingRate(this.game.worldClock) * 100) + "%"
                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);
                table.appendChild(tr);
            });
        });
    }
}