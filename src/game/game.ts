import { ColorUtils } from "../utils/color";
import { GameDataLoader } from "./io/gameDataLoader";
import { GameEvent } from "./events/gameEvent";
import { Country } from "./histories/country";
import { Player } from "./players/player";
import { AI } from "./players/ai";
import { RendererOption } from "../gameData/rendererOption";
import { JsonDeserializer } from "../parser/jsonDeserializer";
import { Scenario } from "../gameData/scenario";

export class Game
{
    private gameDataLoader: GameDataLoader = new GameDataLoader();
    public rendererOption: RendererOption;
    public scenarios: Array<Scenario> = new Array<Scenario>();
    public scenarioID: number = -1;
    public player: Player | null = null;
    public AIs: Array<AI> = new Array<AI>();
    public worldClock: Date = new Date();
    public isInitialized: boolean = false;

    constructor()
    {
        this.rendererOption = JsonDeserializer.DeserializeUIDataToRendererOption(this.gameDataLoader.getUIData());
        this.scenarios = JsonDeserializer.DeserializeScenariosDataToScenarios(this.gameDataLoader.getScenariosData());

        // set random color
        this.scenarios.forEach((v) => v.countries.forEach((c) => c.setColorStr(ColorUtils.getRandomColorStr())));
    }

    public initializeGame(scenarioID: number, playerCountryCode: string)
    {
        // create player and ai players
        // player
        var playerCountry = this.scenarios[scenarioID].countries.find((v) => v.getCode() == playerCountryCode);
        var playerEvents = new Array<GameEvent>();

        this.scenarios[scenarioID].events.forEach((v) =>
        {
            if (v.occurCountryCode == playerCountryCode ||
                v.occurCountryCode == "")
                playerEvents.push(v);
        });

        if (playerCountry != undefined)
            this.player = new Player(playerCountry, playerEvents);

        else
        {
            console.error("Failed to initialize game");
            return;
        }

        // ai
        this.AIs = new Array<AI>();
        var aiCountries = new Array<Country>();
        var aiEvents = new Array<GameEvent>();

        this.scenarios[scenarioID].countries.forEach((v) =>
        {
            if (v.getCode() != playerCountryCode)
                aiCountries.push(v);
        });

        this.scenarios[scenarioID].events.forEach((v) =>
        {
            if (v.occurCountryCode != playerCountryCode &&
                v.occurCountryCode != "")
                aiEvents.push(v);
        });

        aiCountries.forEach((v) =>
        {
            var events = new Array<GameEvent>();

            aiEvents.forEach((e) =>
            {
                if (e.occurCountryCode == v.getCode())
                    events.push(e);
            });

            this.AIs.push(new AI(v, events));
        });

        this.worldClock = this.scenarios[scenarioID].worldClock;
        this.scenarioID = scenarioID;
        this.isInitialized = true;
    }
}