import { ScenariosData } from "../../jsonTypeDefine/scenarios.json";
import { UIData } from "../../jsonTypeDefine/ui.json";

export class GameDataLoader
{
    private uiData: UIData;
    private scenariosData: ScenariosData;

    constructor()
    {
        this.uiData = require("../../gameData/default/ui.json") as UIData;
        this.scenariosData = require("../../gameData/default/scenarios.json") as ScenariosData;
    }

    public getUIData(): UIData
    {
        return this.uiData;
    }

    public getScenariosData(): ScenariosData
    {
        return this.scenariosData;
    }
}