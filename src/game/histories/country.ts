import { Government } from "./governments/government";
import { Province } from "./province";

export class Country
{
    private code: string;
    private name: string;
    private colorStr: string = "";
    private government: Government;
    private provinces: Array<Province>;
    private flagName: string;
    private population: number;
    private nationalStability: number;
    private nationalHappiness: number;
    private economyPower: number;
    private money: number;
    private taxRate: number = 0;

    constructor(code: string,
                name: string,
                government: Government,
                provinces: Array<Province>,
                flagName: string = "unknown.png",
                nationalStability: number = 0.5,
                nationalHappiness: number = 0.5,
                economyPower: number = 0,
                money: number = 1000)
    {
        this.code = code;
        this.name = name;
        this.government = government;
        this.provinces = provinces;
        this.flagName = flagName;
        this.population = 4000000 * this.provinces.length;
        this.nationalStability = nationalStability;
        this.nationalHappiness = nationalHappiness;
        this.economyPower = economyPower;
        this.money = money;
    }

    public getCode(): string
    {
        return this.code;
    }

    public getName(): string
    {
        return this.name;
    }

    public setName(name: string): void
    {
        this.name = name;
    }

    public getColorStr(): string
    {
        return this.colorStr;
    }

    public setColorStr(colorStr: string): void
    {
        this.colorStr = colorStr;
    }

    public getGovernment(): Government
    {
        return this.government;
    }

    public getProvinces(): Array<Province>
    {
        return this.provinces;
    }

    public getFlagName(): string
    {
        return this.flagName;
    }

    public setFlagName(flagName: string): void
    {
        this.flagName = flagName;
    }

    public getPopulation(): number
    {
        return this.population;
    }

    public addPopulation(population: number): void
    {
        this.population += population;
    }

    public getNationalStability(): number
    {
        return this.nationalStability;
    }

    public addNationalStability(value: number): void
    {
        this.nationalStability += value;
    }

    public getNationalHappiness(): number
    {
        return this.nationalHappiness - this.taxRate;
    }

    public addNationalHappiness(nationalHappiness: number): void
    {
        this.nationalHappiness += nationalHappiness;
    }

    public setMonthlyBudget():void
    {
        this.money += this.money * this.taxRate;
    }

    public getMoney(): number
    {
        return this.money;
    }

    public useMoney(money: number): void
    {
        this.money -= money;
    }

    public addMoney(money: number): void
    {
        this.money += money;
    }

    public getEconomyPower(): number
    {
        return this.economyPower;
    }

    public addEconomyPower(economyPower: number): void
    {
        this.economyPower += economyPower;
    }

    public getTaxRate(): number
    {
        return this.taxRate;
    }

    public setTaxRate(taxRate: number): void
    {
        this.taxRate = taxRate;
    }
}