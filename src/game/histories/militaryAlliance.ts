export class MilitaryAlliance
{
    public code: string;
    public name: string;
    public membersCode: Array<string>;

    constructor(code: string, name: string, membersCode: Array<string>)
    {
        this.code = code;
        this.name = name;
        this.membersCode = membersCode;
    }
}