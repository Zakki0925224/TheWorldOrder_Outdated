export class Province
{
    private id: string;
    private name: string;
    private maxLabel: number = 10;
    private infrastructureLevel: number;
    private residenceLevel: number;
    private factoryLevel: number;
    private policeStationLevel: number;
    private hospitalLevel: number;
    private buildingInfo: Array<BuildingInfo> = new Array<BuildingInfo>();

    constructor(id: string,
                name: string,
                infrastructureLevel: number = 0,
                residenceLevel: number = 0,
                factoryLevel: number = 0,
                policeStationLevel: number = 0,
                hospitalLevel: number = 0)
    {
        this.id = id;
        this.name = name;
        this.infrastructureLevel = infrastructureLevel;
        this.residenceLevel = residenceLevel;
        this.factoryLevel = factoryLevel;
        this.policeStationLevel = policeStationLevel;
        this.hospitalLevel = hospitalLevel;
    }

    public getId(): string
    {
        return this.id;
    }

    public getName(): string
    {
        return this.name;
    }

    public setName(newName: string)
    {
        this.name = newName;
    }

    public getMaxLevel(): number
    {
        return this.maxLabel;
    }

    public build(building: Buildings, startDate: Date)
    {
        var buildingDays = 0;

        switch (building)
        {
            case Buildings.INFRASTRUCTURE:
                buildingDays = Buildings.getBuildDays(Buildings.INFRASTRUCTURE);
                break;

            case Buildings.RESIDENCE:
                buildingDays = Buildings.getBuildDays(Buildings.RESIDENCE);
                break;

            case Buildings.FACTORY:
                buildingDays = Buildings.getBuildDays(Buildings.FACTORY);
                break;

            case Buildings.POLICE_STATION:
                buildingDays = Buildings.getBuildDays(Buildings.POLICE_STATION);
                break;

            case Buildings.HOSPITAL:
                buildingDays = Buildings.getBuildDays(Buildings.HOSPITAL);
                break;
        }

        this.buildingInfo.push(new BuildingInfo(building, startDate, buildingDays));
    }

    public getBuildingRate(building: Buildings, now: Date): number
    {
        var rate = -1;
        var b = this.buildingInfo.find((v) => v.getBuilding() == building);

        if (b)
            rate = b.getBuildingRate(now);

        return rate;
    }

    public dailyUpdate(now: Date)
    {
        var removeId = -1;

        for (var i = 0; i < this.buildingInfo.length; i++)
        {
            if (this.buildingInfo[i].getBuildingRate(now) >= 1)
            {
                switch (this.buildingInfo[i].getBuilding())
                {
                    case Buildings.INFRASTRUCTURE:
                        this.infrastructureLevel++;
                        break;

                    case Buildings.RESIDENCE:
                        this.residenceLevel++;
                        break;

                    case Buildings.FACTORY:
                        this.factoryLevel++;
                        break;

                    case Buildings.POLICE_STATION:
                        this.policeStationLevel++;
                        break;

                    case Buildings.HOSPITAL:
                        this.hospitalLevel++;
                        break;
                }

                removeId = i;
            }
        }

        if (removeId != -1)
            this.buildingInfo.splice(removeId, 1);
    }

    public getInfrastructureLevel(): number
    {
        return this.infrastructureLevel;
    }

    public getResidenceLevel(): number
    {
        return this.residenceLevel;
    }

    public getFactoryLevel(): number
    {
        return this.factoryLevel;
    }

    public getPoliceStationLevel(): number
    {
        return this.policeStationLevel;
    }

    public getHospitalLevel(): number
    {
        return this.hospitalLevel;
    }

    public getBuildingInfo(): Array<BuildingInfo>
    {
        return this.buildingInfo;
    }
}

export enum Buildings
{
    INFRASTRUCTURE,
    RESIDENCE,
    FACTORY,
    POLICE_STATION,
    HOSPITAL
}

export namespace Buildings
{
    export function toString(buildings: Buildings): string
    {
        switch (buildings)
        {
            case Buildings.INFRASTRUCTURE:
                return "Infrastructure";

            case Buildings.RESIDENCE:
                return "Residence";

            case Buildings.FACTORY:
                return "Factory";

            case Buildings.POLICE_STATION:
                return "PoliceStation";

            case Buildings.HOSPITAL:
                return "Hospital";
        }
    }

    export function getBuildCost(buildings: Buildings): number
    {
        switch (buildings)
        {
            case Buildings.INFRASTRUCTURE:
                return 1;

            case Buildings.RESIDENCE:
                return 2;

            case Buildings.FACTORY:
                return 3;

            case Buildings.POLICE_STATION:
                return 2;

            case Buildings.HOSPITAL:
                return 10;
        }
    }

    export function getMonthlyCost(buildings: Buildings): number
    {
        switch (buildings)
        {
            case Buildings.INFRASTRUCTURE:
                return 2;

            case Buildings.RESIDENCE:
                return 1;

            case Buildings.FACTORY:
                return 5;

            case Buildings.POLICE_STATION:
                return 5;

            case Buildings.HOSPITAL:
                return 8;
        }
    }

    export function getBuildDays(buildings: Buildings): number
    {
        switch (buildings)
        {
            case Buildings.INFRASTRUCTURE:
                return 183;

            case Buildings.RESIDENCE:
                return 548;

            case Buildings.FACTORY:
                return 365;

            case Buildings.POLICE_STATION:
                return 183;

            case Buildings.HOSPITAL:
                return 730;
        }
    }
}

class BuildingInfo
{
    private building: Buildings;
    private startDate: Date;
    private endDate: Date = new Date();

    constructor(building: Buildings, startDate: Date, buildingDays: number)
    {
        var endDate = new Date(Date.UTC(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getUTCDate(), startDate.getUTCHours()));
        endDate.setUTCDate(endDate.getUTCDate() + buildingDays);
        this.building = building;
        this.startDate = new Date(Date.UTC(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getUTCDate(), startDate.getUTCHours()));
        this.endDate = endDate;
    }

    public getBuilding(): Buildings
    {
        return this.building;
    }

    public getStartDate(): Date
    {
        return this.startDate;
    }

    public getEndDate(): Date
    {
        return this.endDate;
    }

    public getBuildingRate(now: Date): number
    {
        var diff = (now.valueOf() - this.startDate.valueOf()) / (1000 * 60 * 60 *24);

        switch (this.building)
        {
            case Buildings.INFRASTRUCTURE:
                return diff / Buildings.getBuildDays(Buildings.INFRASTRUCTURE);

            case Buildings.RESIDENCE:
                return diff / Buildings.getBuildDays(Buildings.RESIDENCE);

            case Buildings.FACTORY:
                return diff / Buildings.getBuildDays(Buildings.FACTORY);

            case Buildings.POLICE_STATION:
                return diff / Buildings.getBuildDays(Buildings.POLICE_STATION);

            case Buildings.HOSPITAL:
                return diff/ Buildings.getBuildDays(Buildings.HOSPITAL);
        }
    }
}