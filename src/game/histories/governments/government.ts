import { Party } from "./party";

export class Government
{
    public party: Party;

    constructor(party: Party = new Party())
    {
        this.party = party;
    }
}