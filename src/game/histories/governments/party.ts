export class Party
{
    public name: string;
    public leaderName: string;
    public leaderPortraitName: string;

    constructor(name: string = "Unknown Party",
                leaderName: string = "Unknown Leader",
                leaderPortraitName: string = "unknown.png")
    {
        this.name = name;
        this.leaderName = leaderName;
        this.leaderPortraitName = leaderPortraitName;
    }
}