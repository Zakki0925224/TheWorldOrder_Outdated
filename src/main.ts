import { Game } from "./game/game";
import { GameRenderer } from "./game/renderers/gameRenderer";
import { TitleRenderer } from "./game/renderers/titleRenderer";

interface Window { game: Game, gameRenderer: GameRenderer, titleRenderer: TitleRenderer }
declare var window: Window;

export class Main
{
    constructor()
    {
        window.game = new Game();
        window.gameRenderer = new GameRenderer(window.game);
        window.titleRenderer = new TitleRenderer(window.game);
    }
}

var main = new Main();