import { GameEvent } from "../game/events/gameEvent";
import { Country } from "../game/histories/country";
import { Government } from "../game/histories/governments/government";
import { Party } from "../game/histories/governments/party";
import { Province } from "../game/histories/province";
import { EffectInfo, Effects } from "../game/players/effect";
import { RendererOption } from "../gameData/rendererOption";
import { Scenario } from "../gameData/scenario";
import { Effect, ScenariosData } from "../jsonTypeDefine/scenarios.json";
import { UIData } from "../jsonTypeDefine/ui.json";
import { ScriptedDate } from "./scriptedDate";

export class JsonDeserializer
{
    public static DeserializeUIDataToRendererOption(data: UIData): RendererOption
    {
        return new RendererOption(data.color.ui_container,
                                  data.color.map.ocean,
                                  data.color.map.field,
                                  data.color.map.border,
                                  data.color.map.fill_color_opacity);
    }

    public static DeserializeScenariosDataToScenarios(data: ScenariosData): Array<Scenario>
    {
        var scenarios = new Array<Scenario>();

        data.scenarios.forEach((v) =>
        {
            var countries = new Array<Country>();
            v.countries.forEach((c) =>
            {
                var gov = c.government;
                var party = new Party(gov?.party?.party_name, gov?.party?.leader, gov?.party?.leader_portrait_path);
                var government = new Government(party);
                var provinces = new Array<Province>();
                c.provinces.forEach((p) => provinces.push(new Province(p, p)));

                countries.push(new Country(c.code, c.name, government, provinces, c.flag_name, c.population));
            });

            var events = new Array<GameEvent>();

            v.events.forEach((e) =>
            {
                var effects = new Array<EffectInfo>();
                if (effects) effects = this.DeserializeToEffects(e.effects!);
                events.push(new GameEvent(e.id, ScriptedDate.parseToUTCDate(e.occur_date), e.occur_country_code, e.title, e.text,e.button_text, effects));
            });

            var worldClock = ScriptedDate.parseToUTCDate(v.start_date);
            var scenario = new Scenario(v.title, countries, events, worldClock);
            scenarios.push(scenario);
        });

        return scenarios;
    }

    private static DeserializeToEffects(effects: Array<Effect>): Array<EffectInfo>
    {
        var ei = new Array<EffectInfo>();
        var effectKeys = Object.keys(Effects).filter((k: any) => typeof Effects[k] === "number");

        effects.forEach((v) =>
        {
            var index = -1;

            for (var i = 0; i < effectKeys.length; i++)
            {
                if (effectKeys[i] == v.key)
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
                ei.push(new EffectInfo(index, v.value));
        });

        return ei;
    }
}