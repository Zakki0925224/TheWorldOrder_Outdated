export class ScriptedDate
{
    public static parseToUTCDate(str: string): Date
    {
        var local = new Date(str);
        return new Date(Date.UTC(local.getUTCFullYear(), local.getUTCMonth(), local.getUTCDate(), local.getUTCHours(), local.getUTCMinutes()));
    }
}